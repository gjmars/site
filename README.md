# Installation

installer les dépendances:

    sudo apt install ruby-dev zlib1g-dev
    bundler update
    bundle install --path vendor/bundle


# Lancement

Lancer le serveur local avec:

    bundler exec jekyll serve


Le site sera disponible sur http://127.0.0.1:4000/site/


# Liens

- Documentation jekyll: https://jekyllrb.com/docs/installation/
- Le site est déployé sur https://gjmars.frama.io/site/
- Le thème utilisé est https://mmistakes.github.io/minimal-mistakes/
