---
layout: single
title: Ressources et Liens
permalink: /ressources/
toc: true
---


## Les sites

### International

* [ONU](https://news.un.org/fr/story/2019/02/1036341)

### National

* [Assemblée des Gilets Jaunes](https://assembleesdesgiletsjaunes.fr/) - Démarrage en cours
* [Gilet Jaune France](https://giletjaune-france.fr/) - En attente de rapporteurs, portes-paroles, communicants ou représentants de groupes locaux de gilets jaunes du 13
* [Gilets Jaunes - portail collaboratif](http://www.giletsjaunes-coordination.fr)
* [Carte des groupes](https://www.giletsjaunes-coordination.fr/carte.html), et [de nombreuses ressources proposées](http://www.giletsjaunes-coordination.fr/liens.html)
* [Les Gilets Jaune de Rouen](https://www.gj-magazine.com/gj/qui-sommes-nous/)
* [Charte proposée par François Boulo - projet](https://www.dropbox.com/s/x36jewd4s4g0b4m/Charte%20commune%20gilets%20jaunes.pdf?dl=0)
* [Site Jaune Français - Anglais - Allemand](https://jaune.site/) et des idées de mise en place d'une [banque libre](https://jaune.site/fr/banque.html)
* [TV Gilets Jaunes - nombreuses vidéos](https://tvgiletsjaunes.com/)
* [Comité de Liaison pour l’Initiative Citoyenne](https://clic-ric.org/)
* [Radio Parleur](https://radioparleur.net/)

![Tract GJ-Magazin.com](https://i0.wp.com/www.gj-magazine.com/gj/wp-content/uploads/2019/04/gjmag2final.jpg?w=1264&ssl=1)

### PACA

Retrouvez les infos sur les [Pages Gilets Jaunes](https://www.pagesgiletsjaunes.fr/provence-alpes-cote-d-azur/13-bouches-du-rhone/)


### Marseille


* [Communiqué de la coordination contre la répression à propos de la manif du 26 janvier - Marseille Infos Autonomes](https://mars-infos.org/communique-de-la-coordination-3824)
* [BMVR - Bibliothèques de Marseille](https://www.bmvr.marseille.fr/geosearch/c837d91e-1634-46bb-9fd4-6b568d947eb1)



## Les groupes facebook

**ATTENTION ! Les données enregistrées sur facebook ne sont pas protégées. Le contenu, une fois publié ne vous appartient plus. Le gouvernement traque les groupes FB. De plus les pages peuvent être effacées sans préavis.**


### National

* [Un annuaire des groupes facebook régionaux ou nationaux](https://www.gilets-jaunes.com/)
* [Gilet Jaune Blog](https://www.facebook.com/pg/GiletJauneblog/posts/?ref=page_internal)
* Conseil National De La Résistance Des Gilets Jaunes

### PACA

* [Union Gilets Jaunes 84](https://fr-fr.facebook.com/uniongiletsjaunes84/)
* [Gilet Jaune VAR](https://fr-fr.facebook.com/pages/category/Band/Gilet-Jaune-VAR-504219263405099/)


### Marseille

* [MARSEILLE GILET JAUNE](https://www.facebook.com/groups/926540050877272)
* GILETS JAUNES BDR
* Gilets Jaunes Marseille centre-ville
* Les giles jaunes de marseille camp de saint loup
* [Gilets Jaunes Historique Aix en Provence. Groupe fermé](https://www.facebook.com/groups/2230054077253296/)
* [GILETS JAUNES MARSEILLE SUD](https://www.facebook.com/groups/GiletsJaunesMarseille/)
* [Gilets jaunes Marseille](https://fr-fr.facebook.com/pages/category/Band/Gilets-jaunes-Marseille-366532950748124/)
* [Gilet Jaune Marseille](https://fr-fr.facebook.com/pages/category/Cause/Gilet-Jaune-Marseille-288271838482683/)



## Les comptes twitter

* [JaunesTvNews](https://twitter.com/JaunesTvNews)



## Les ressources thématique


### Juridique


Consulter [la partie Juridique](../juridique/)


### RIC


* [Le Vrai Débat](https://www.le-vrai-debat.fr/) avec le [groupe PACA](https://paca.plateforme-gilets-jaunes.fr/)
* [Wiki RIC](https://wikiric.com/)
* [Le RIC sur Parlement et Citoyens](https://parlement-et-citoyens.fr/consultation/referendum-dinitiative-citoyenne/presentation/presentation-18) - consultation effectée en déc 18/jan 19, en attente des résultats
* [Participer à des ateliers constituants](https://jecrislaconstitution.fr/) - inscription nécessaire



### Démocratie participative


* [Expérience de vote en ligne en 2017 Laprimaire.org (50 000 votes enregistrés)](https://laprimaire.org/) : Le processus de LaPrimaire.org comportait de nombreuses innovations: [un vote en ligne basé sur la blockchain ethereum, l’utilisation de lots aléatoires et du jugement majoritaire](https://articles.laprimaire.org/laprimaire-org-cest-reparti-3d06750f6526)
* [Liste de site autour de la Civic Tech](http://wearereadynow.net/wp-content/uploads/2016/09/Guidedemobilisation.pdf)



### Référendum

* [France Référendum](https://www.france-referendum.fr/referendum/soutenez-vous-le-mouvement-des-gilets-jaune)
* [Collecticity](https://www.collecticity.fr/referendum-en-ligne/) - Co-construisez aujourd'hui
le service public de demain


### Numérique

Des alternatives voient le jour, les Services Internet Libres Ethiques Décentralisés Ouverts et Solidaires :
* [Le projet degooglisons internet de Framasoft](http://degooglisons-internet.org/)
* La liste des alternatives : services en ligne libres et éthiques (framasoft et autres) : [Annuaire du libre : framalibre](https://framalibre.org/)
* [Les services en ligne libres de marsnet (Marseille)](http://marslibre.org/)


### Gilet Jaunes outils libres

*Sur les réseaux sociaux libres et décentralisés, respectueux des données et de la vie privée, alternatives éthiques aux services des GAFAM, voici quelques nœuds réseau jaunes, ou tag réseau jaunes sur des nœuds (la liste n'est pas exhaustive, puisque chacun peut en créer un :)*

* [Un compte Gilets Jaunes sur le "twitter libre" : Mastodon](https://framapiaf.org/tags/giletsjaunes)
* [Des comptes Gilets Jaunes sur le "facebook libre" : Diaspora](https://framasphere.org/tags/giletsjaunes)
* [Des vidéos Gilets Jaunes sur le "youtube libre" : Peertube](https://peertube.fr/search?search=giletsjaunes)


et quelques liens utiles pour utiliser diaspora :

* [Tuto](https://diasporafoundation.org/tutorials)
* [Code de conduite](https://diasporafoundation.org/community_guidelines)
* [Mise en forme du texte - utiliser Markdown](https://diasporafoundation.org/formatting)
* [Kit de base Diaspora](https://framasphere.org/posts/e865188033670132f129061787dcf416)
* [Diaspora : Le guide du parfait débutant](https://fr.wikibooks.org/wiki/Diaspora_:_Le_guide_du_parfait_d%C3%A9butant)
* [Un compte Gilets Jaunes marseillais sur le "facebook libre" : Diaspora](https://framasphere.org/people/2281f9e0394601378e2c2a0000053625)
* [Réseau jaune](https://reseaujaune.com/)


*Les réseaux sociaux libres sont utilisés par 2.5 millions de personnes, en croissance exponentielle ([source](https://the-federation.info/)). Ce sont les réseaux sociaux du peuple par le peuple, pas ceux des multinationales qui gouvernent ce monde, en faisant du profit avec nos données. Utiliser les réseaux sociaux libres c'est les renforcer.*


## Les collectifs, les groupes

* [L'affreux Jojo : journal du groupe gilet jaune de la région nantaise](https://laffreuxjojo.home.blog/)


## Les auteurs, les personnages

* [Blog de Juan Branco](http://branco.blog.lemonde.fr/)
* [Livre Crepuscule - version pdf gratuite](http://branco.blog.lemonde.fr/files/2019/01/Macron-et-son-Crepuscule.pdf)


Et beaucoup de pages sur les gilets jaunes :

* [Via Lilo - moteur de recherche qui soutient les projets](https://search.lilo.org/searchweb.php?q=gilets%20jaunes)
* [Via Ecosia - moteur de recherche qui plante des arbres](https://www.ecosia.org/search?q=gilets+jaunes)






[![tract 3](../medias/images/tract3.jpg)](../juridique)
