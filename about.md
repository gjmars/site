---
layout: page
title: About
permalink: /about/
---
[accueil](/site/)
[about](/site/about/)
[juridique](/site/juridique/)

Ceci est le nouveau site des gilets jaunes de marseille



# INFOS JURIDIQUES

Infos juridiques récupérées sur https://soutien13gj.wixsite.com/home/infos-juridiques-utiles

## Pendant une manif :

* https://mars-infos.org/filme-un-flic-sauve-une-vie-petit-3757
* https://www.giletau.org/vos-droits-de-manifestant/

## Pendant une manif et GAV :

* http://gjinfo.org/conseils-manif/

## Pendant une GAV :

* http://gjinfo.org/garde-a-vue-avocats-gilets-jaunes/
* http://gjinfo.org/nos-droit-en-garde-a-vue/




This is the base Jekyll theme. You can find out more info about customizing your Jekyll theme, as well as basic Jekyll usage documentation at [jekyllrb.com](https://jekyllrb.com/)

You can find the source code for Minima at GitHub:
[jekyll][jekyll-organization] /
[minima](https://github.com/jekyll/minima)

You can find the source code for Jekyll at GitHub:
[jekyll][jekyll-organization] /
[jekyll](https://github.com/jekyll/jekyll)


[jekyll-organization]: https://github.com/jekyll
