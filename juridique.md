---
layout: single
title: Informations juridiques
permalink: /juridique/
toc: true
---


## Droits et devoirs du manifestant :

* [Vos droits de manifestant](https://www.giletau.org/vos-droits-de-manifestant/)
* [Résumé des droits, par la CNT - Confédération nationale du travail (syndicat)](http://www.cnt-f.org/les-droits-du-manifestant.html)
* [Conseils Manif, par gjinfo.org](http://gjinfo.org/conseils-manif/)
* [Contrôle de sécurité : fouille corporelle, vérification d'un sac, du véhicule, sur service-public.fr](https://www.service-public.fr/particuliers/vosdroits/F32041)
* [Gilets jaunes, citoyens, droit de manifester et autres droits, L'Humanité](https://www.humanite.fr/gilets-jaunes-interpellation-garde-vue-comparution-immediate-vous-avez-des-droits-664826)
* [Flyer Manif PDF, par Legal Team](https://mars-infos.org/home/chroot_ml/ml-marseille/ml-marseille/public_html/IMG/pdf/lt-mars.pdf)
* [Manifestants : Droits et conseils en cas d'interpellation](http://lesaf.org/manifestantes-droits-et-conseils-en-cas-dinterpellation/)



## Pendant la manifestation

**Attention ! Les policiers n'aiment pas être filmés. Aussi faites le à 2 : une personne qui filme, une personne qui filme celle qui filme ;)**

* ["Filme un flic sauve une vie"](https://mars-infos.org/filme-un-flic-sauve-une-vie-petit-3757)
* [Une manif n'est pas une promenade de santé, par Rebellyon.info](https://rebellyon.info/Petit-guide-juridique-Une-manif-n)
* [Face à la police](http://cntait89.free.fr/Pratique/face_a_la_police.htm)
* [Face à la police / Face à la justice, par Mille Babords](http://www.millebabords.org/spip.php?article7076)



## Manuel de survie en garde à vue et en comparution immédiate

* [L'essentiel à savoir, par Le Poing](https://lepoing.net/manuel-de-survie-en-garde-a-vue-et-en-comparution-immediate/)
* [Guide GAV très complet](https://expansive.info/home/chroot_ml/ml-expansive/ml-expansive/public_html/IMG/pdf/garde_a_vue_-_recettes_theoriques_et_pratiques_-_legal_team_zad_dec._2017_a5.pdf)
* [Les 10 commandements en GAV, DEFENSE COLLECTIVE](https://defensecollective.noblogs.org/dc-comics/les-10-commandements-en-gav/)
* [Nos droits en garde à vue](http://gjinfo.org/nos-droit-en-garde-a-vue/)



## Avocats

* [Avocats Gilets Jaunes, sur gjinfo](http://gjinfo.org/garde-a-vue-avocats-gilets-jaunes/)
* Marseille - Avocat GJ : Thierry Murdy

Maître Thierry MUDRY est un Avocat au Barreau & Ordre des Avocats de MARSEILLE 6EME ARR. (13006) depuis 2003

*Contact : 06.09.27.20.64*  
*e-Mail : thierry.murdy@orange.fr*  
*Adresse : 18 C rue Jules Moulet - 13006 Marseille*


* [LEGAL TEAM MARSEILLE](https://mars-infos.org/contact-legal-team-3390)

e-Mail: legalteam-marseille@riseup.net


| **Nom** | **Tél** |
|---|---|
| *N° d'urgence* | 07.53.24.35.31 |
| Me Marc Wahed | 04.91.98.96.58 |
| Me Cécile Bernhard | 04.91.52.30.25 |
| Me Thomas Vartanian | 06.27.75.16.21 |
| Me Philippe Chaudon | 06.24.48.42.76 |


* LEGAL TEAM AIX


| **Nom** | **Tél** |
|---|---|
| Me Auberi Salecroix | 06.81.08.18.94 |
| Me Marc Breard | 06.99.09.50.00 |
| Me Julie Mulateri | 06.26.89.40.09 |
| Me Cindy Frigerio | 06.17.08.10.89 |
| Me Hubert Claudie | 06.60.29.33.61 |
| Me Julien Gautier | 04.42.93.33.50 / 06.31.46.40.64 |




## Vidéos

* Droits des street medics, question sur l'évacuation des rond-point, question sur les amendes 135 euros pour port de GJ ou t-shirt RIC, manif interdites, dispersion après sommation, entrave à la circulation, rassemblement/attroupement différence et conséquences : [La super avocate d'Avignon](https://www.youtube.com/watch?v=5QB3Ka2_rCw&feature=share)

* [Evacuation terrain, interpellations ou empêchement de manifester, droit de filmer](https://www.youtube.com/watch?v=Nmb7nZdo2vI)



## Pour témoigner

* [Vos droits - service public](https://www.service-public.fr/particuliers/vosdroits/R11307)
* [Attestation de témoin - CERFA](https://cerfa.blog/2018/12/11/cerfa-n1152703-attestation-de-temoin/)
* [Signalement IGPN pour victimes et témoins mais ne constitue pas une plainte](http://www.police-nationale.interieur.gouv.fr/Organisation/Inspection-Generale-de-la-Police-Nationale/Signalement-IGPN)
* [Dépôt pré-plainte en ligne](http://www.pre-plainte-en-ligne.gouv.fr)
* [Saisie du défenseur des droits](http://defenseurdesdroits.fr)



## Après une GAV :

* [Consultation et effacement des fichiers de police](https://mars-infos.org/consultation-et-effacement-des-3882)
* [Contre le fichage, organisons-nous collectivement](https://mars-infos.org/contre-le-fichage-organisons-nous-3259)



## Pour les prisonniers :

* [Que faire quand un-e proche est incarcéré-e ?](https://mars-infos.org/que-faire-quand-un-e-proche-est-3603)



## Concevoir distribuer flyer

* [Législation distribution de tracts](https://www.giletau.org/wp-content/uploads/asgarosforum/140/l%c3%a9gislation-distribution-de-tracts.pdf)




##### Sources des infos juridiques : [soutien13gj](https://soutien13gj.wixsite.com/home/infos-juridiques-utiles)
