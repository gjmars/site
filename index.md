---
layout: single
title: Accueil
permalink: /
---

**Bienvenue sur le site des Gilets Jaunes de Marseille !**

Ce site recense les liens et ressources importants pour découvrir le mouvement et vous impliquer. Bonne visite :)

[![Accueil](medias/images/tract2.jpg)](a-propos/)
